document.getElementById("input-plot").oninput = function()
{
	document.getElementById("2b").innerHTML = 4 / Math.sin( degToRad( +this.value ) );
};

function degToRad(n)
{
	return Math.PI / 180 * n;
}

function radToDeg(n)
{
	return 180 / Math.PI * n;
};;